import asyncio

async def hello():
    print('¡Hola ...')
    await asyncio.sleep(1)
    print('... mundo!')

async def main():
    await asyncio.gather(hello(), hello(), hello())

# Obtener el bucle
loop = asyncio.get_event_loop()

# Ejecutar la función main en el bucle
try:
    loop.run_until_complete(main())
finally:
    # Cerrar el bucle una vez que se completa la función principal
    loop.close()
