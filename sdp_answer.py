import asyncio
import json
import sdp_transform  # Asegúrate de que sdp-transform está instalado

class SDPAnswerServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        # Decodificar el mensaje recibido
        message = data.decode()
        print("Received message:\n", message)

        # Intentar deserializar el SDP recibido
        try:
            offer_sdp = json.loads(message)
        except json.JSONDecodeError:
            print("Received message is not valid JSON.")
            return

        # Verificar la existencia de la clave 'media'
        if 'media' not in offer_sdp or not isinstance(offer_sdp['media'], list):
            print("Received SDP does not contain 'media' key or it's not a list.")
            return

        # Cambiar el puerto en la sección 'media'
        try:
            offer_sdp['media'][0]['port'] = 34543  # Cambiando puerto de audio
            offer_sdp['media'][1]['port'] = 34543  # Cambiando puerto de video
        except (IndexError, KeyError) as e:
            print(f"Error modifying 'media' section: {e}")
            return

        # Generar la respuesta SDP
        answer_sdp = sdp_transform.write(offer_sdp)
        print('Sending SDP Answer:\n', answer_sdp)
        self.transport.sendto(answer_sdp.encode(), addr)

async def main():
    print("Starting UDP server for SDP")

    loop = asyncio.get_running_loop()

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SDPAnswerServerProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Run for 1 hour
    finally:
        transport.close()

asyncio.run(main())

