import asyncio
import json
import sdp_transform

class SDPAnswerServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message_json = json.loads(data.decode())
        offer_sdp = sdp_transform.parse(message_json['sdp'])

        # Modificar la oferta SDP según sea necesario
        offer_sdp['media'][0]['port'] = 34543  # Cambio de puerto de audio
        offer_sdp['media'][1]['port'] = 34543  # Cambio de puerto de video

        answer_sdp = sdp_transform.write(offer_sdp)
        answer_json = json.dumps({"type": "answer", "sdp": answer_sdp})
        self.transport.sendto(answer_json.encode(), addr)

    def connection_lost(self, exc):
        print("Connection closed")
        if exc:
            print(f"Error: {exc}")

    def error_received(self, exc):
        print('Error received:', exc)

async def main():
    loop = asyncio.get_running_loop()

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SDPAnswerServerProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Servidor en funcionamiento durante 1 hora
    finally:
        transport.close()

asyncio.run(main())
